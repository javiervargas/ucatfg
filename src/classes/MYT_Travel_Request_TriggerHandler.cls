/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Travel_Request_TriggerHandler
* @Author Javier Vargas Gerona
* @Date Created: 2020-03-16 
* @Group Trigger Handlers
* @Description This class manages the Trigger execution for travel request and its methods
* @Changes
*
* |2020-03-16 
* First version
*/
public with sharing class MYT_Travel_Request_TriggerHandler extends MYT_TriggerHandler{

	private List<MYT_Travel_Request__c> lstNewReq;
	private List<MYT_Travel_Request__c> lstOldReq;
	private Map<Id, MYT_Travel_Request__c> mapNewReq;
	private Map<Id, MYT_Travel_Request__c> mapOldReq;

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona
     * @Date Created: 2020-03-16 
     * @Description Constructor
     **/
	public MYT_Travel_Request_TriggerHandler() {
		this.lstNewReq = (List<MYT_Travel_Request__c>) Trigger.new;
		this.mapNewReq = (Map<Id, MYT_Travel_Request__c>) Trigger.newMap;
		this.mapOldReq = (Map<Id, MYT_Travel_Request__c>) Trigger.oldMap;
		this.lstOldReq = (List<MYT_Travel_Request__c>) Trigger.old;
	}

	/**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona
     * @Date Created: 2020-03-16 
     * @Description after insert method of the trigger
     **/
    public override void afterInsert() {
		for(MYT_Travel_Request__c travelReq : lstNewReq) {
            MYT_SendGrid_Service.sendSingleDynamicEmailTemplateFuture(travelReq.Id, travelReq.SG_Email_TemplateId__c, travelReq.MYT_Email_Address__c);
        }

    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona
     * @Date Created: 2020-03-16 
     * @Description before insert method of the trigger
     **/
    public override void beforeInsert() {
        SG_Email_Template__c template = [SELECT Id, SG_Teamplate_Id__c FROM SG_Email_Template__c LIMIT 1];
		for(MYT_Travel_Request__c travelReq : lstNewReq) {
            travelReq.MYT_Email_Template__c = template.Id;
            travelReq.SG_Email_TemplateId__c = template.SG_Teamplate_Id__c;
            Location loc1 = Location.newInstance(travelReq.MYT_Origin_coordinates__Latitude__s,travelReq.MYT_Origin_coordinates__Longitude__s);
            Location loc2 = Location.newInstance(travelReq.MYT_Destination_coordinates__Latitude__s,travelReq.MYT_Destination_coordinates__Longitude__s);
            travelReq.MYT_Distance_between_places__c = Location.getDistance(loc1, loc2, 'km');
            if(travelReq.MYT_Travel_Duration__c == null) {
                travelReq.MYT_Travel_Duration__c = (travelReq.MYT_Departure_Date__c).daysBetween(travelReq.MYT_Return_Date__c);
            }
            travelReq.MYT_Travel_Value__c = (travelReq.MYT_Budget__c - (travelReq.MYT_Distance_between_places__c * 0.05)) / travelReq.MYT_Travel_Duration__c;
        }
    }
    
}