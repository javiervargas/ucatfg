/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Share_Selector
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-04-14
* @Group Selector
* @Description Selector Class for shares Records
*/
public with sharing class MYT_Share_Selector {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-04-14
     * @Description insert a List of MYT_Travel_Request__Share
     * @param List<MYT_Travel_Request__Share> reqShares
     **/
    public static void insertReqShares(List<MYT_Travel_Request__Share> reqShares) {
        insert reqShares;
    }
}