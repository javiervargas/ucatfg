/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_HomeForm_ctrl
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-01-16
* @Group Controller
* @Description Controller class for home form
*/
public with sharing class MYT_HomeForm_ctrl {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author AJavier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-01-16
     * @Description Insert a Lead and his travel request associated
     * @param Lead leadForm
     * @param MYT_Travel_Request__c requestForm
     * @return Boolean
     **/
    @AuraEnabled
    public static Boolean saveForm(Lead leadForm, MYT_Travel_Request__c requestForm) {
        return MYT_HomeForm_service.saveForm(leadForm, requestForm);
    }
}