/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_SendProposals_Schedulable 
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-04-10
* @Group Schedulable 
* @Description Schedulable class. Send pending proposals 
*/
public with sharing class MYT_SendProposals_Schedulable Implements Schedulable {
    /** 
    * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    * @Author avier Vargas Gerona javier.vargas.gerona@gmail.com
    * @Date 2020-04-10
    * @Description This method executes schedulable context calling 
    * @param SchedulableContext sc
    **/
    public void execute(SchedulableContext sc) {
    	Id batchJobId = Database.executeBatch(new MYT_SendProposals_Batch(), 200);
    }
}