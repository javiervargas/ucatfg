/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_HomeForm_service
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-01-16
* @Group Service
* @Description Service class for home form
*/
public with sharing class MYT_HomeForm_service {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-01-16
     * @Description Insert a Lead and his travel request associated
     * @param Lead leadForm
     * @param MYT_Travel_Request__c requestForm
     * @return Boolean
     **/
    public static Boolean saveForm(Lead leadForm, MYT_Travel_Request__c requestForm) {
        try {
            // Initialize a list to hold any duplicate records
            List<sObject> duplicateRecords = new List<sObject>();

            Database.SaveResult saveResult = Database.insert(leadForm, false);

            if (!saveResult.isSuccess()) {
                for (Database.Error error : saveResult.getErrors()) {
                    // If there are duplicates, an error occurs
                    // Process only duplicates and not other errors 
                    //   (e.g., validation errors)
                    if (error instanceof Database.DuplicateError) {
                        // Handle the duplicate error by first casting it as a 
                        //   DuplicateError class
                        // This lets you use methods of that class 
                        //  (e.g., getDuplicateResult())
                        Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                        Datacloud.DuplicateResult duplicateResult =  duplicateError.getDuplicateResult();

                        duplicateResult.getErrorMessage();
                        
                        // Get duplicate records
                        duplicateRecords = new List<sObject>();

                        // Return only match results of matching rules that 
                        //  find duplicate records
                        Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();

                        // Just grab first match result (which contains the 
                        //   duplicate record found and other match info)
                        Datacloud.MatchResult matchResult = matchResults[0];

                        Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                        // Add matched record to the duplicate records variable
                        for (Datacloud.MatchRecord matchRecord : matchRecords) {
                            duplicateRecords.add(matchRecord.getRecord());
                        }
                    }
                }
            }
            // If duplicateRecords is empty, leadForm has been inserted 
            Id leadId = duplicateRecords.isEmpty() ? leadForm.Id : duplicateRecords[0].Id;
            requestForm.MYT_Lead__c = leadId;
            requestForm.MYT_Email_Address__c = leadForm.Email;
            requestForm.MYT_Travel_Locator__c = String.valueOf(System.now());
            insert requestForm;
            return true;
        } catch (Exception e) {
            System.debug(e.getMessage());
            return false;
        }
        
    }
}