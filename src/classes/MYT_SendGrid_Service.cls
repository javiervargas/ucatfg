/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_SendGrid_Service
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-02-04
* @Group Service
* @Description Service class for sendgrid functions
*/
public with sharing class MYT_SendGrid_Service {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description send dynamic Email using Send Grid API
     * @param Id whatEmail
     * @param String templateId
     * @param String emailAddress
     **/
    public static void sendSingleDynamicEmailTemplate(Id whatEmail, String templateId, String emailAddress) {

        MYT_SendGrid_Helper.EmailTemplateWrapper templateWrapper;
        Map<String,Set<String>> objectsFields = new Map<String,Set<String>>();
        Map<String,String> mergeFields = new Map<String,String>();
        String reqBody;
        MYT_SendGrid_Helper.Personalizations personali = new MYT_SendGrid_Helper.personalizations();
        
        personali.to                    = MYT_SendGrid_Helper.setToPersonalization(emailAddress);
        templateWrapper                 = MYT_SendGrid_Helper.getEmailTemplate(templateId);
        objectsFields                   = MYT_SendGrid_Helper.retrieveFieldsFromTemplate(templateWrapper);
        personali.dynamic_template_data = MYT_SendGrid_Helper.getMergeFields(whatEmail, objectsFields);
        reqBody                         = MYT_SendGrid_Helper.createSendTemplateRequestBody(new List<MYT_SendGrid_Helper.Personalizations>{personali}, templateId);
        HttpResponse response           = MYT_SendGrid_Helper.sendEmail(reqBody);
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description send dynamic Email using Send Grid API future
     * @param Id whatEmail
     * @param String templateId
     * @param String emailAddress
     **/
    @future(callout=true)
    public static void sendSingleDynamicEmailTemplateFuture(Id whatEmail, String templateId, String emailAddress) {
        sendSingleDynamicEmailTemplate(whatEmail,templateId, emailAddress);
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-04-08
     * @Description send Email with specific personalizations parameters
     * @param List<MYT_SendGrid_Helper.Personalizations> emailPersonalizations
     * @param String templateId
     **/
    public static HttpResponse sendEmailWithPersonalizations(List<MYT_SendGrid_Helper.Personalizations> emailPersonalizations, String templateId) {

        String reqBody          = MYT_SendGrid_Helper.createSendTemplateRequestBody(emailPersonalizations, templateId);
        HttpResponse response   =  MYT_SendGrid_Helper.sendEmail(reqBody);
        return response;
    }
}