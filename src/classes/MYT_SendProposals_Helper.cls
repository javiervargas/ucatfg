/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_SendProposals_Helper
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-04-09
* @Group Helper
* @Description Helper class for send Proposal Batch
*/
public with sharing class MYT_SendProposals_Helper {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description relate Travel proposals with his travel requests in a Map
     * @param List<MYT_Travel_Proposal__c> proposals
     * @return Map<String,List<MYT_Travel_Proposal__c>>
     **/
    public static Map<String,List<MYT_Travel_Proposal__c>> getMapReqIds(List<MYT_Travel_Proposal__c> proposals) {
        Map<String,List<MYT_Travel_Proposal__c>> mapReqsWithProps = new Map<String,List<MYT_Travel_Proposal__c>>();
        for(MYT_Travel_Proposal__c prop : proposals) {
            if(mapReqsWithProps.containsKey(prop.MYT_Travel_Request__c)) {
                mapReqsWithProps.get(prop.MYT_Travel_Request__c).add(prop);
            } else {
                mapReqsWithProps.put(prop.MYT_Travel_Request__c, new List<MYT_Travel_Proposal__c>{prop});
            }
            prop.MYT_Sended__c = true;
        }
        return mapReqsWithProps;
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description return a Map for personalizations section to sendGrid API call
     * @param List<MYT_Travel_Request__c> requests
     * @param Map<String,List<MYT_Travel_Proposal__c>> mapReqsWithProps
     * @return List<MYT_SendGrid_Helper.Personalizations>
     **/
    public static List<MYT_SendGrid_Helper.Personalizations> formatPersonalizations(List<MYT_Travel_Request__c> requests, Map<String,List<MYT_Travel_Proposal__c>> mapReqsWithProps) {
        
        List<MYT_SendGrid_Helper.Personalizations> emailPersonalizations = new List<MYT_SendGrid_Helper.Personalizations>();

        for(MYT_Travel_Request__c req : requests) {
            MYT_SendGrid_Helper.Personalizations personaliz = new MYT_SendGrid_Helper.personalizations();
            Map<String, String> dynamicValues = new Map<String, String>();
            personaliz.to = MYT_SendGrid_Helper.setToPersonalization(req.MYT_Email_Address__c);
            dynamicValues.put('firstName', req.MYT_Lead__r.FirstName);
            dynamicValues.put('locator', req.MYT_Travel_Locator__c);
            dynamicValues.put('numPropuestas', String.valueOf(mapReqsWithProps.get(req.Id).size()));
            personaliz.dynamic_template_data = dynamicValues;
            emailPersonalizations.add(personaliz);
        }

        return emailPersonalizations;

    }

    
}