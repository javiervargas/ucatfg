/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_SG_Email_Template_Selector
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-04-08
* @Group Selector
* @Description Selector Class class for SG_Email_Template
*/
public with sharing class MYT_SG_Email_Template_Selector {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-04-08
     * @Description get email template given Id
     * @param String templateId
     * @return SG_Email_Template__c
     **/
    public static SG_Email_Template__c getTemplateBySGId(String templateId) {
        return [SELECT Id, Name, MYT_Email_Template_Json__c FROM SG_Email_Template__c WHERE SG_Teamplate_Id__c =: templateId WITH SECURITY_ENFORCED LIMIT 1];
    }
}