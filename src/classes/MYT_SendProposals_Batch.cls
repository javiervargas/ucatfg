/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_SendProposals_Batch
* @Author Javier Vargas
* @Date Created: 20120-04-09
* @Group Batches
* @Description Batch class. send pending proposals
*/

public with sharing class MYT_SendProposals_Batch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    public String logType;

    /**
    * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    * @Author
    * @Date 20119-02-13
    * @Description Empty Constructor, initialize parameters
    **/
    public MYT_SendProposals_Batch(){

    }
    
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas
     * @Date 20119-02-13
     * @Description This method collects the batches of records or objects to be passed to execute
     * @param Database.BatchableContext .
     * @return Database.QueryLocator
     **/
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return MYT_Travel_Proposal_Selector.getPendingProposals();
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas
     * @Date 20119-02-13
     * @Description This method processes each batch of records
     * @param List<Account> 
     **/
    public void execute(Database.BatchableContext bc, List<MYT_Travel_Proposal__c> scope){
		MYT_SendProposals_Service.sendProposals(scope);
    } 

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas
     * @Date  20119-02-13
     * @Description This method executes post-processing operations
     * @param Database.BatchableContext 
     **/  
    @SuppressWarnings('PMD.EmptyStatementBlock')
    //Supress warning to avoid EmptyStatementBlock in this framework
    public void finish(Database.BatchableContext bc){

       
    }    
}