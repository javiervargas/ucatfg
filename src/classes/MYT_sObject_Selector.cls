/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_sObject_Selector
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-02-07
* @Group Helper
* @Description Helper class for sendgrid functions
*/
public with sharing class MYT_sObject_Selector {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-07
     * @Description get a sObject record given his Id, object Name and List of fields to retrieve
     * @param 
     * @param 
     * @return
     **/
    public static sObject getObjectByIdWithFields(Id recordId, String objectName, Set<String> fields) {

        fields.remove('Id');
        String query = 'SELECT Id ';
        for (String field : fields) {
            query += ', '+field;
        }
        query += ' FROM ' + objectName + ' WHERE Id = :recordId LIMIT 1';
        
        sObject toReturn = Database.query(query);

        return toReturn;

    }
}