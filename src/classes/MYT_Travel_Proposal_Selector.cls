/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Travel_Proposal_Selector
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-03-27
* @Group Selector
* @Description Selector por Travel Proposal Object 
*/
public with sharing class MYT_Travel_Proposal_Selector {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get travel proposal given the id and the ownerId
     * @param String requestId
     * @param String userId
     * @return List<MYT_Travel_Proposal__c>
     **/
    public static List<MYT_Travel_Proposal__c> getProposalByOwnerRequestId(String requestId, String userId) {
        return [SELECT Id, Name, MYT_Ready_to_Send__c, MYT_Travel_Request__r.Name FROM MYT_Travel_Proposal__c WHERE MYT_Travel_Request__c = :requestId AND OwnerId = :userId WITH SECURITY_ENFORCED];
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get Query locator for ready to send proposals
     * @return Database.QueryLocator
     **/
    public static Database.QueryLocator getPendingProposals() {
        return Database.getQueryLocator('SELECT Id, MYT_Travel_Request__c FROM MYT_Travel_Proposal__c WHERE MYT_Ready_to_Send__c = true AND  MYT_Sended__c = false WITH SECURITY_ENFORCED');
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description insert proposal
     * @param MYT_Travel_Proposal__c newProposal
     * @return MYT_Travel_Proposal__c
     **/
    public static MYT_Travel_Proposal__c insertProposal(MYT_Travel_Proposal__c newProposal) {
        insert newProposal;
        return newProposal;
    }
}