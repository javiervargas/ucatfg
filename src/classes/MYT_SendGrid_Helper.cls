/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_SendGrid_Helper
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-02-04
* @Group Helper
* @Description Helper class for sendgrid functions
*/
public with sharing class MYT_SendGrid_Helper {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description retrieve an email template from SendGrid
     * @param String templateSGId
     * @return EmailTemplateWrapper
     **/
    public static EmailTemplateWrapper retrieveEmailTemplate(String templateSGId) {

        String endpoint = Label.MYT_SG_BaseURL + 'templates/' + templateSGId;
        HttpResponse response = makeCallout(endpoint,null,null, 'GET');
        EmailTemplateWrapper templateWrapper = parseTemplate(response.getBody());

        return templateWrapper;

    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description retrieve an email template from SendGrid
     * @param String templateSGId
     * @return EmailTemplateWrapper
     **/
    public static EmailTemplateWrapper getEmailTemplate(String templateSGId) {

        String templateJSON = MYT_SG_Email_Template_Selector.getTemplateBySGId(templateSGId).MYT_Email_Template_Json__c;
        EmailTemplateWrapper templateWrapper = parseTemplate(templateJSON);

        return templateWrapper;

    }
    
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description Insert a Lead and his travel request associated
     * @param EmailTemplateWrapper template
     * @return Map<String,Set<String>>
     **/
    public static Map<String,Set<String>> retrieveFieldsFromTemplate(EmailTemplateWrapper template) {

        Map<String,Set<String>> objectsFields = new Map<String,Set<String>>();
        String fields = template.versions[0].test_data;
        Map<String,String> fieldsWithObject = (Map<String,String>)JSON.deserialize(fields, Map<String,String>.class);
        
        for (String field : fieldsWithObject.keySet()) {
            String fieldName;
            List<String> fieldPath = field.split('DDD');
            String objectName = fieldsWithObject.get(field);
            if (fieldPath.size() == 1) {
                fieldName =  field;
            } else {
                fieldName = fieldPath[1];
                objectName = fieldPath[0] + 'DDD' + objectName;
            } 
            if (objectsFields.containsKey(objectName)) {
                Set<String> flds = objectsFields.get(objectName);
                flds.add(fieldName);
                objectsFields.put(objectName, flds);
            } else {
                objectsFields.put(objectName, new Set<String>{fieldName});
            }
        }

        return objectsFields;
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-06
     * @Description get values of merge fields template
     * @param Id recordId
     * @param Map<String,Set<String>> fieldsWithObject
     * @return Map<String, String>
     **/
    public static Map<String, String> getMergeFields(Id recordId, Map<String,Set<String>> fieldsWithObject) {

        Map<String,String> mergeFields = new Map<String,String>();
        
        String mainObjectName = recordId.getSObjectType().getDescribe().getName();
        Set <String> mainFields = new Set<String>();
        mainFields.addAll(fieldsWithObject.get(mainObjectName));
        Set<String> additionalMainFields = new Set<String>();
        for (String objectName : fieldsWithObject.keySet()){
            if(objectName.split('DDD').size() > 1){
                additionalMainFields.add(objectName.split('DDD')[0]);
            }
        }
        mainFields.addAll(additionalMainFields);

        sObject mainSObject = MYT_sObject_Selector.getObjectByIdWithFields(recordId, mainObjectName, mainFields);
        for (String field : fieldsWithObject.get(mainObjectName)) {
            mergeFields.put(field, String.valueOf(mainSObject.get(field)));
        }

        fieldsWithObject.remove(mainObjectName);

        for (String objectItem : fieldsWithObject.keySet()) {
            List<String> vs = objectItem.split('DDD');
            String lookupField = vs[0];
            String objectName = vs[1];

            sObject secondSObject = MYT_sObject_Selector.getObjectByIdWithFields((Id)mainSObject.get(lookupField), objectName, fieldsWithObject.get(objectItem));
            for (String field : fieldsWithObject.get(objectItem)) {
                mergeFields.put(lookupField+'DDD'+field, String.valueOf(secondSObject.get(field)));
            }
        }

        return mergeFields;
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-07
     * @Description return wrapper for body send email request
     * @param List<Personalizations> emailPersonalizations
     * @param String templateId
     * @return String
     **/
    public static String createSendTemplateRequestBody(List<Personalizations> emailPersonalizations, String templateId) {

        SendEmailGridWrapper sendWrapper = new SendEmailGridWrapper();
        fromZZ from_z = new fromZZ();
        from_z.email = 'javier.vargas.gerona@gmail.com';
        from_z.name = 'Make Your Travel';
        sendWrapper.fromZZ = from_z;
        sendWrapper.template_id = templateId;
        sendWrapper.personalizations = emailPersonalizations;
        String body = JSON.serialize(sendWrapper);
        body = body.replace('ZZ', '');
        
        return body;
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description send email callout given the body request
     * @param String body
     * @return HttpResponse
     **/
    public static HttpResponse sendEmail(String body) {

        String endpoint = Label.MYT_SG_BaseURL + 'mail/send';
        Map<String,String> mapHeaders = new Map<String,String>();
        mapHeaders.put('Content-Type', 'application/json');
        HttpResponse response = makeCallout(endpoint,mapHeaders,body, 'POST');

        return response;

    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-04
     * @Description make callout given all necesary parameters
     * @param String endpoint
     * @param Map<String,String> headers
     * @param String body
     * @param String callMethod
     * @return HttpResponse
     **/
    private static HttpResponse makeCallout(String endpoint, Map<String,String> headers, String body, String callMethod) {

        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod(callMethod);
        request.setHeader('Authorization', 'Bearer ' + Label.MYT_SG_ApiKey);
        if (headers != null) {
            for(String headerType : headers.keySet()) {
                request.setHeader(headerType, headers.get(headerType));
            }
        }
        System.debug('Headers: '+ request.getHeader('Content-Type'));
        if (body != null) {
            request.setBody(body);
        }

        HttpResponse response = h.send(request);

        return response;
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-04-08
     * @Description set single destination email
     * @param String emailAddress
     * @return List<MYT_SendGrid_Helper.To>
     **/
    public static List<MYT_SendGrid_Helper.To> setToPersonalization(String emailAddress) {
        To to_z = new To();
        to_z.email = emailAddress;
        return new List<To>{to_z};
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-04-08
     * @Description set multiple destination emails
     * @param List<String> emailAddresses
     * @return List<MYT_SendGrid_Helper.To>
     **/
    public static List<MYT_SendGrid_Helper.To> setToPersonalization(List<String> emailAddresses) {
        List<To> destinations = new List<To>();
        for(String emailAddress : emailAddresses) {
            To to_z = new To();
            to_z.email = emailAddress;
            destinations.add(to_z);
        }   
        return destinations;
    }

    /* response TEMPLATE WRAPPER  */
	public class EmailTemplateWrapper {
		public String id;
		public String name;
		public String generation;
		public String updated_at;
		public List<Versions> versions;
	}

    public class Versions {
		public String id;
		public Integer user_id;
		public String template_id;
		public Integer active;
		public String name;
		public String html_content;
		public String plain_content;
		public Boolean generate_plain_content;
		public String subject;
		public String updated_at;
		public String editor;
		public String test_data;
		public String thumbnail_url;
	}

	
	public static EmailTemplateWrapper parseTemplate(String json) {
		return (EmailTemplateWrapper) System.JSON.deserialize(json, EmailTemplateWrapper.class);
    }

    /* END response TEMPLATE WRAPPER  */
    

    /* request SEND EMAIL WRAPPER  */
    public class SendEmailGridWrapper {
		public FromZZ fromZZ;
		public List<Personalizations> personalizations;
		public String template_id;
		public List<Content> content;
	}

	public class Content {
		public String typeZZ;
		public String value;
	}

	public class To {
		public String email;
	}

	public class Personalizations {
		public List<To> to;
		public Map<String,String> dynamic_template_data;
	}

	public class FromZZ {
		public String email;
		public String name;
	}

	
	public static SendEmailGridWrapper parse(String json) {
		return (SendEmailGridWrapper) System.JSON.deserialize(json, SendEmailGridWrapper.class);
    }
    /* END request SEND EMAIL WRAPPER  */
}