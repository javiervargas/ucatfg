/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Travel_Request_Selector
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-03-28
* @Group Selector
* @Description Selector por Travel Request Object 
*/
public with sharing class MYT_Travel_Request_Selector {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-28
     * @Description get a travel request given the id
     * @param String requestId
     * @return MYT_Travel_Request__c
     **/
    public static MYT_Travel_Request__c getRequestRT(String requestId) {
        return [SELECT Id, RecordType.Name FROM MYT_Travel_Request__c WHERE Id =: requestId WITH SECURITY_ENFORCED LIMIT 1];
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-28
     * @Description get travel requests given a set of ids
     * @param Set<String> requestIds
     * @return List<MYT_Travel_Request__c>
     **/
    public static List<MYT_Travel_Request__c> getRequestsByIds(Set<String> requestIds) {
        return [SELECT Id, MYT_Email_Address__c, MYT_Lead__r.FirstName, MYT_Origin_city__c, MYT_Destination_city__c, MYT_Travel_Locator__c
                FROM MYT_Travel_Request__c WHERE Id IN: requestIds WITH SECURITY_ENFORCED];
    }
}