
/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Group_Selector
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-04-14
* @Group Selector
* @Description Selector Class for Groups
*/
public with sharing class MYT_Group_Selector {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-04-14
     * @Description get app configuration by name
     * @param Set<String> groupNames
     **/
    public static List<Group> getConfigByNameSet(Set<String> groupNames) {
        return [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN: groupNames WITH SECURITY_ENFORCED];
    }
}