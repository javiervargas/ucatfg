/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_SendProposals_Service
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-04-09
* @Group Service
* @Description Service class for send Proposal Batch
*/
public with sharing class MYT_SendProposals_Service {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-02-09
     * @Description send email for new proposals alerts
     * @param List<MYT_Travel_Proposal__c> proposals
     **/
    public static void sendProposals(List<MYT_Travel_Proposal__c> proposals) {
        Map<String,List<MYT_Travel_Proposal__c>> mapReqsWithProps = new Map<String,List<MYT_Travel_Proposal__c>>();
        List<MYT_Travel_Request__c> requests = new List<MYT_Travel_Request__c>();
        List<MYT_SendGrid_Helper.Personalizations> emailPersonalizations = new List<MYT_SendGrid_Helper.Personalizations>();
        String reqBody;
        HttpResponse response;

        mapReqsWithProps        = MYT_SendProposals_Helper.getMapReqIds(proposals);
        requests                = MYT_Travel_Request_Selector.getRequestsByIds(mapReqsWithProps.keySet());
        emailPersonalizations   = MYT_SendProposals_Helper.formatPersonalizations(requests,mapReqsWithProps);
        response                = MYT_SendGrid_Service.sendEmailWithPersonalizations(emailPersonalizations, 'd-a116e1ee356e45bba83c3a4ddd09e81a');
        
        if(response.getStatusCode() == 202){
            update proposals;
        }
    }
}