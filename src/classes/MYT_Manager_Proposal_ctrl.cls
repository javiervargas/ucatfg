/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Manager_Proposal_ctrl
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-03-27
* @Group Controller
* @Description Controller class for Manager Proposal cmp
*/
public with sharing class MYT_Manager_Proposal_ctrl {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get content
     * @param String requestId
     * @return MYT_Travel_Proposal__c
     **/
    @AuraEnabled
    public static MYT_Travel_Proposal__c getProposal(String requestId) {
        System.debug('requestId: '+requestId);
        return MYT_Manager_Proposal_Service.getProposal(requestId);
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get content
     * @param SObject requestId
     * @return SObject
     **/
    @AuraEnabled
    public static SObject saveProposal(String proposal) {
        System.debug('proposal: '+proposal);
        return MYT_Manager_Proposal_Service.saveProposal(proposal);
    }
}