/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Configuration_Selector
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-04-14
* @Group Selector
* @Description Selector Class for App Config Metadata
*/
public with sharing class MYT_Configuration_Selector {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-04-14
     * @Description get app configuration by name
     * @param String configName
     **/
    public static MYT_App_Config__mdt getConfigByName(String configName) {
        return [SELECT Id, MYT_Config_JSON__c FROM MYT_App_Config__mdt WHERE DeveloperName =: configName WITH SECURITY_ENFORCED LIMIT 1];
    }
}