/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Manager_Proposal_Service
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-03-27
* @Group Service
* @Description Service class for Manager Proposal cmp
*/
public with sharing class MYT_Manager_Proposal_Service {
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get content
     * @param String requestId
     * @return Boolean
     **/
    public static MYT_Travel_Proposal__c getProposal(String requestId) {
        MYT_Travel_Proposal__c proposal = new MYT_Travel_Proposal__c();
        proposal = MYT_Manager_Proposal_Helper.getProposalByOwnerRequestId(requestId);
        if (proposal == null) {
            proposal = MYT_Manager_Proposal_Helper.initializeProposal(requestId);
        }
        return proposal;
    }
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get content
     * @param String requestId
     * @return Boolean
     **/
    public static SObject saveProposal(String proposal) {

        MYT_Travel_Proposal__c newProp = MYT_Manager_Proposal_Helper.populateFields(proposal);
        MYT_Travel_Proposal_Selector.insertProposal(newProp);
        return newProp;
    }
}