/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name MYT_Manager_Proposal_Helper
* @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
* @Date Created: 2020-03-28
* @Group Helper
* @Description Helper class for Manager Proposal cmp
*/
public with sharing class MYT_Manager_Proposal_Helper {
    
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get content
     * @param String requestId
     * @return Boolean
     **/
    public static MYT_Travel_Proposal__c getProposalByOwnerRequestId(String requestId) {
        List<MYT_Travel_Proposal__c> proposal = MYT_Travel_Proposal_Selector.getProposalByOwnerRequestId(requestId, UserInfo.getUserId());
        if(proposal.size() > 0) {
            return proposal[0];
        } else {
            return null;
        }
        
    }
    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get content
     * @param String requestId
     * @return Boolean
     **/
    public static MYT_Travel_Proposal__c initializeProposal(String requestId) {
        MYT_Travel_Proposal__c proposal = new MYT_Travel_Proposal__c();
        String requestRT = MYT_Travel_Request_Selector.getRequestRT(requestId).RecordType.Name;
        proposal.RecordTypeId = Schema.SObjectType.MYT_Travel_Proposal__c.getRecordTypeInfosByName().get(requestRT).getRecordTypeId();
        return proposal;
    }

    /**
     * -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
     * @Author Javier Vargas Gerona javier.vargas.gerona@gmail.com
     * @Date 2020-03-27
     * @Description get content
     * @param String requestId
     * @return Boolean
     **/
    public static MYT_Travel_Proposal__c populateFields(String proposal) {
        MYT_Travel_Proposal__c newProp = new MYT_Travel_Proposal__c();
        Map<String,Object> propfields = (Map<String,Object>)JSON.deserializeUntyped(proposal);
        for(String field: propfields.keySet()) {
            newProp.put(field,propfields.get(field));
        }
        return newProp;
    }
}