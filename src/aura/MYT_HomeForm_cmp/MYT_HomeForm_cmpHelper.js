({
    handleSend : function(cmp) {
        const residenceInputcmp = cmp.find('residenceInputcmp');
        const toInputcmp = cmp.find('toInputcmp');
        const fromInputcmp = cmp.find('fromInputcmp');
        
        cmp.set("v.newLead.City", residenceInputcmp.get('v.location'));
        cmp.set("v.newLead.Country", residenceInputcmp.get('v.country'));

        cmp.set("v.newRequest.MYT_Origin_city__c", fromInputcmp.get('v.location'));
        cmp.set("v.newRequest.MYT_Origin_coordinates__Latitude__s", fromInputcmp.get('v.latitude'));
        cmp.set("v.newRequest.MYT_Origin_coordinates__Longitude__s", fromInputcmp.get('v.longitude'));
        cmp.set("v.newRequest.MYT_Destination_city__c", toInputcmp.get('v.location'));
        cmp.set("v.newRequest.MYT_Destination_coordinates__Latitude__s", toInputcmp.get('v.latitude'));
        cmp.set("v.newRequest.MYT_Destination_coordinates__Longitude__s", toInputcmp.get('v.longitude'));


        var action = cmp.get('c.saveForm'); 
        action.setParams({
            'leadForm' : cmp.get('v.newLead'),
            'requestForm' : cmp.get('v.newRequest')
        });
        action.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var resp = response.getReturnValue();
                if(resp) {
                    cmp.set("v.formSended", true);
                }
            }
        });
        $A.enqueueAction(action);
    }
})