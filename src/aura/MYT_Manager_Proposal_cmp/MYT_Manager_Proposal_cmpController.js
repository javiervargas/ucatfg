({
    doInit : function(cmp, event, helper) {
        helper.doInit(cmp);
    },
    editClic : function(cmp, event, helper) {
        if(cmp.get('v.formMode') == 'edit') {
            cmp.set('v.formMode', 'view');
        }
        cmp.set('v.formMode', 'edit');
        cmp.set('v.showButtons', false);
        cmp.set('v.editclicked', true);
    },
    createClic : function(cmp, event, helper) {
        cmp.set('v.createclicked', true);
    },
    onLoadHandler : function(cmp, event, helper) {
        if(cmp.get('v.editclicked')) {
            cmp.set('v.editclicked', false);
        } else {
            cmp.set('v.showButtons', true);
        }
    },
    onSubmitCreateHandler: function(cmp, event, helper) {
        helper.onhandleCreateSubmit(cmp,event);
    }, 
    showModal: function(cmp, event, helper) {
        cmp.set('v.showModal', true);
    },
    hideModal: function(cmp, event, helper) {
        cmp.set('v.showModal', false);
    },
    sendProposal: function(cmp, event, helper) {
        helper.sendProposal(cmp);
    }
})