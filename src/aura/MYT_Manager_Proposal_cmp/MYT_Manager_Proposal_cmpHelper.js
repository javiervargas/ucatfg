({
    doInit : function(cmp) {

        var action = cmp.get('c.getProposal'); 
        action.setParams({
            'requestId' : cmp.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var resp = response.getReturnValue();
                if(resp !== null) {
                    cmp.set("v.proposalRecord", resp);
                    cmp.set("v.showSpinner", false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    onhandleCreateSubmit: function(cmp, event) {
        event.preventDefault();
        const eventFields = event.getParam('fields');
        eventFields.MYT_Travel_Request__c = cmp.get('v.recordId');
        var action = cmp.get('c.saveProposal'); 
        action.setParams({
            'proposal' : JSON.stringify(eventFields)
        });
        action.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var resp = response.getReturnValue();
                if(resp !== null) {
                    cmp.set("v.proposalRecord", resp);
                    cmp.set("v.showSpinner", false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    sendProposal: function(cmp) {
        const prop = cmp.get('v.proposalRecord');
        let fields = '{"Id":"'+prop.Id+'","MYT_Ready_to_Send__c":true}';
        cmp.find('proposalToSend').submit(JSON.parse(fields));
        prop.MYT_Ready_to_Send__c = true;
        cmp.set('v.proposalRecord', prop);
        cmp.set('v.showModal', false);
    },
})