({
    callgetSuggestions : function(cmp, event, helper){	
        var actiongetSuggestions = cmp.get("c.getSuggestions");
        
        actiongetSuggestions.setParams({
            "input": cmp.get('v.location')
        }); 

        actiongetSuggestions.setCallback(this, function(response) {
            var state = response.getState();
            // console.log(response.getState());
            if(state == "SUCCESS"){
                // console.log('resp', response.getReturnValue());
                var resp = JSON.parse(response.getReturnValue());	
                // console.log('resp.predictions',resp.predictions);
                cmp.set('v.predictions',resp.predictions);	
            } else {
                console.log("Failed with state: " + state);   
            }
        });
        $A.enqueueAction(actiongetSuggestions); 
     
    },

    callgetPlaceDetails : function(cmp, event, helper){	
	 
        var actiongetPlaceDetails = cmp.get("c.getPlaceDetails");
        
        actiongetPlaceDetails.setParams({
            "placeId": event.currentTarget.dataset.placeid
        });

        actiongetPlaceDetails.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                console.log('resp', response.getReturnValue());
                var placeDetails = JSON.parse(response.getReturnValue()); 
                let address_components = placeDetails.result.address_components;
                cmp.set('v.location',placeDetails.result.name);	
                cmp.set('v.latitude',placeDetails.result.geometry.location.lat);	
                cmp.set('v.longitude',placeDetails.result.geometry.location.lng);	
                Object.keys(address_components).forEach(function(key) {
                    if (address_components[key].types[0] === 'country') {
                        cmp.set('v.country',address_components[key].long_name);
                        cmp.set('v.countryShort',address_components[key].short_name);	
                    }
                });
                cmp.set('v.predictions',[]);
                console.log('resp', placeDetails.result.name);
            } else {
                console.log("Failed with state: " + state);   
            }
        });
        $A.enqueueAction(actiongetPlaceDetails); 
     
    }
})