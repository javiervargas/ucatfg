trigger MYT_Travel_Request_Trigger on MYT_Travel_Request__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    //if (CC_ConfigurationService.isTriggerActive('CC_AccountTrigger')) {
        new MYT_Travel_Request_TriggerHandler().run();
    //}
}