<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Plan Assignator</label>
    <protected>false</protected>
    <values>
        <field>MYT_Config_JSON__c</field>
        <value xsi:type="xsd:string">{
&quot;MYT_Gold_users&quot;:9999,
&quot;MYT_Silver_Users&quot;:150,
&quot;MYT_Standard_Users&quot;:120
}</value>
    </values>
</CustomMetadata>
