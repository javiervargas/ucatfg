declare module "@salesforce/apex/MYT_HomeForm_ctrl.saveForm" {
  export default function saveForm(param: {leadForm: any, requestForm: any}): Promise<any>;
}
