declare module "@salesforce/apex/MYT_GoogleApis_ctrl.getSuggestions" {
  export default function getSuggestions(param: {input: any}): Promise<any>;
}
declare module "@salesforce/apex/MYT_GoogleApis_ctrl.getPlaceDetails" {
  export default function getPlaceDetails(param: {placeId: any}): Promise<any>;
}
